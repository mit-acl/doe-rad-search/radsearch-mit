import requests, sys

page = requests.get('https://api.github.com/repos/uzh-rpg/flightmare/releases/latest', allow_redirects=True)
response = page.json()

if sys.argv[1] == "version":
    print(response['name'])
elif sys.argv[1] == "url":
    print(response['assets'][0]['browser_download_url'])

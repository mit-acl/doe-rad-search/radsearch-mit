#!/bin/bash

usage()
{
cat << EOF
usage: update.sh [CMD]

CMD options:
  * dependencies
    - Will install system dependencies. Assumes you already have ROS
      desktop version installed.
  * packages
    - Will recursively update git submodules in packages/ and make sure
      their package dependencies are up-to-date.
  * flightmare
    - Will check https://github.com/uzh-rpg/flightmare/releases for the
      newest Unity environment release and give the option to download.
  * flightmare-pointer.
    - Will give the option to change the flightmare executable your sim
      points to.
  * apriori-path PLANNER_NAME SETTINGS_NAME
    - Will use PLANNER_NAME (callable from [packages/trajectory_generation/
      planners/PLANNER_NAME/main.py]) and SETTINGS_NAME (callable from 
      [packages/radsearch_config/param/planning/PLANNER_NAME/SETTINGS_NAME.yaml]) 
      to generate waypoint file [packages/trajectory_generation/waypoints/
      PLANNER_NAME-SETTINGS_NAME.txt] and video file [logs/
      PLANNER_NAME-SETTINGS_NAME.mp4].

EOF
}

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
scriptdir="$DIR/scripts"
packagedir="$DIR/packages"
unitydir="$DIR/unity_builds"
logsdir="$DIR/logs"
evaluationdir="$DIR/evaluation"

update_dependencies()
{
echo "Installing system dependencies..."

# Flightmare dependencies
sudo apt-get install -y --no-install-recommends build-essential cmake libzmqpp-dev libopencv-dev
sudo apt-get install -y libgoogle-glog-dev protobuf-compiler ros-$ROS_DISTRO-octomap-msgs ros-$ROS_DISTRO-octomap-ros ros-$ROS_DISTRO-joy
if [[ "$(uname -v)" == *20.04* ]]; then
  sudo apt-get install -y python3-vcstool
else
  sudo apt-get install -y python-vcstool
fi
sudo apt-get install -y python-pip
sudo pip install catkin-tools

# Snap dependencies
sudo apt install -y libncurses5-dev

# TrajOpt dependencies
sudo apt install ros-$ROS_DISTRO-timed-roslaunch

# Kimera dependencies
sudo apt-get install -y ros-$ROS_DISTRO-image-geometry ros-$ROS_DISTRO-pcl-ros ros-$ROS_DISTRO-cv-bridge ros-$ROS_DISTRO-cmake-modules
sudo apt-get install -y --no-install-recommends apt-utils
sudo apt-get install -y \
      cmake build-essential unzip pkg-config autoconf \
      libboost-all-dev \
      libjpeg-dev libpng-dev libtiff-dev
if [[ "$(uname -v)" == *16.04* ]]; then
  sudo apt-get install -y libvtk5-dev libgtk2.0-dev
else
  sudo apt-get install -y libvtk6-dev libgtk-3-dev
fi
sudo apt-get install -y libatlas-base-dev gfortran libparmetis-dev
if [[ "$(uname -v)" == *20.04* ]]; then
  sudo apt-get install -y python3-wstool
else
  sudo apt-get install -y python-wstool
fi
sudo apt-get install -y libtbb-dev

# A priori planning dependencies
pip3 install pyyaml
sudo apt-get install ffmpeg
}

update_packages()
{
cd "${packagedir}"
echo "Updating submodules..."
git submodule update --init --recursive
echo "Updating dependencies..."
vcs-import < deps/flightmare_snap_dependencies.yaml
vcs-import < deps/kimera_dependencies.yaml
vcs-import < deps/trajectory_dependencies.yaml
echo "Pulling docker images..."
# ++++
}

update_flightmare()
{
asset_version="$(python3 $scriptdir/get_latest_flightmare.py version)"
asset_url="$(python3 $scriptdir/get_latest_flightmare.py url)"
echo "Latest Flightmare Unity version: $asset_version"
if [ ! -d "$unitydir/$asset_version" ]; then
  read -p "You don't have this version downloaded. Download? [yN] " yn
  case $yn in
    [Yy]*)
      cd "$unitydir"
      echo "Downloading..."
      wget -O $asset_version.tar.xz $asset_url
      echo "Extracting..."
      mkdir $asset_version
      tar -xf $asset_version.tar.xz -C $asset_version
      rm $asset_version.tar.xz
      echo "Done. Version $asset_version downloaded to your workspace. Consider pointing to it with ./update.sh flightmare-pointer."
    ;;
    [Nn]*)
      echo "Not downloading."
    ;;
    *)
      echo "Not downloading."
    ;;
  esac
else
  echo "You already have this version downloaded."
fi
}

update_flightmare_pointer()
{
version_list=()
cd "$unitydir"
echo "Avalailable Flightmare Unity Versions:"
echo ""
for v in *; do
  [ -d $v ] || break
  if [ -d "$packagedir/flightmare/flightrender/RPG_Flightmare" ]; then
    if [[ "$(ls -la $packagedir/flightmare/flightrender/RPG_Flightmare)" == *$v* ]]; then
      echo "  $v *"
    else
      echo "  $v"
    fi
  else
    echo "  $v"
  fi
  version_list+=("$v")
done
echo ""
read -p "Pick the version to point to in your simulation: " point_vers
if [[ " ${version_list[@]} " =~ " ${point_vers} " ]]; then
  echo "Pointing to version $point_vers."
  cd "$packagedir/flightmare/flightrender"
  ln -sfn "$unitydir/$point_vers/" RPG_Flightmare
else
  echo "Invalid version specified."
fi
}

run_apriori_planner()
{
planner_name="$1"
settings_name="$2"

planner_exe="$packagedir/trajectory_generation/planners/${planner_name}/main.py"
yaml_file="$packagedir/radsearch_config/param/planning/${planner_name}/${settings_name}.yaml"
pcl_path="$evaluationdir/pcl"
figure_dir="$logsdir/${planner_name}-${settings_name}-figs"
if [ ! -d "${figure_dir}" ]; then
  mkdir "${figure_dir}"
fi
waypoint_file="$packagedir/trajectory_generation/waypoints/${planner_name}-${settings_name}.txt"
video_file="$logsdir/${planner_name}-${settings_name}.mp4"

echo "Running a priori planner ${planner_name} with settings ${settings_name}..."

"${planner_exe}" --yaml_file "${yaml_file}" --pcl_path "${pcl_path}" --generate_figs 1 --figure_dir "${figure_dir}" --output "${waypoint_file}"

echo "Generating video from output figures..."

ffmpeg -i "$figure_dir/fig_%d.png" -c:v libx264 -vf fps=25 -pix_fmt yuv420p "${video_file}"

rm -rf "${figure_dir}"
}

key="$1"

case $key in
  -h|--help)
    usage
    exit
  ;;
  dependencies)
    update_dependencies
    exit
  ;;
  packages)
    update_packages
    exit
  ;;
  flightmare)
    update_flightmare
    exit
  ;;
  flightmare-pointer)
    update_flightmare_pointer
    exit
  ;;
  apriori-path)
    run_apriori_planner "$2" "$3"
    exit
  ;;
  *)
    usage
    exit
  ;;
esac

usage

#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

echo -e "\033[1;36m=================================================================\033[0m"
echo -e "\033[1;36m===========\033[0m NOTEBOOK URL: http://localhost:8888/tree \033[1;36m============\033[0m"
echo -e "\033[1;36m=================================================================\033[0m"

function ctrl_c() {
docker stop kimera_eval
docker rm kimera_eval
}

trap ctrl_c INT

logspath="$(realpath ../logs/)"
nbspath="$(realpath notebooks)"
parampath="$(realpath ../packages/radsearch_config/param/kimera_sas)"
docker run -it -p 8888:8888 -v "$logspath:/home/sas_evaluation/logs" \
  -v "$nbspath:/home/sas_evaluation" \
  -v "$parampath:/home/sas_evaluation/params" \
  --name kimera_eval goromal/radsearch-kimera-evaluation:SAS /bin/sh -c "/home/start_sas_eval.sh"
docker rm kimera_eval
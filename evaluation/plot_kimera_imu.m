bagfile = '../logs/kimera_imu.bag';
addpath(genpath('matlab_utilities/'));
bagdata = processAllROSBagTopics(bagfile, false);

imu = bagdata.imu.throttled.corrected;

imu_t = imu.t;

acc_x = imu.acc(1,:);
acc_y = imu.acc(2,:);
acc_z = imu.acc(3,:);

gyr_x = imu.gyro(1,:);
gyr_y = imu.gyro(2,:);
gyr_z = imu.gyro(3,:);

subplot(2,1,1)
plot(imu_t, gyr_x, 'b-', imu_t, gyr_y, 'g-', imu_t, gyr_z, 'r-')
grid on

subplot(2,1,2)
plot(imu_t, acc_x, 'b-', imu_t, acc_y, 'g-', imu_t, acc_z, 'r-')


















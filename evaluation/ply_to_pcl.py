#!/usr/bin/python3

import sys, os

plyfilepath = sys.argv[1]
plyfilename = os.path.basename(plyfilepath)
plyfldrname = os.path.dirname(plyfilepath)
pclfilepath = os.path.join(plyfldrname, plyfilename.split('.')[0] + '.txt')

with open(plyfilepath, 'r') as plyfile, open(pclfilepath, 'w') as pclfile:
    plylines = plyfile.readlines()
    numpoints = int(plylines[2].split(' ')[2])
    plylines = plylines[16:numpoints+16]
    print('Writing %d points from %s -> %s...' % (numpoints, plyfilepath, pclfilepath))
    for line in plylines:
        splitline = line.split(' ')
        pclfile.write('%s %s %s\n' % (splitline[0], splitline[1], splitline[2]))
    print('Done.')
pclname = 'sas_pc_0';

addpath(genpath('matlab_utilities'));
pclfile = ['pcl/' pclname '.bin'];
pcldata = read_log(pclfile, 3, false);

skip_num = 100;
subplot(1,2,1)
plot3(pcldata(1,1:skip_num:end),pcldata(2,1:skip_num:end),pcldata(3,1:skip_num:end),'k.')
grid on
xlim([-16 16])
ylim([-16 16])
zlim([0 4])
pbaspect([8 8 1])
title('Full Environment Point Cloud')

subplot(1,2,2)
num_points = size(pcldata, 2);
for i = 1:50:num_points
    if pcldata(3, i) >= 0.5 && pcldata(3,i) <= 0.6
        plot(pcldata(1,i),pcldata(2,i),'k.')
        hold on
    end
end
hold off
pbaspect([1 1 1])
title('Cross Section')
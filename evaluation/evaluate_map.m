
load_data = true;
mapname = 'a_priori_sas-more_conservative-snap_cam';
num_points = 1e5;
bin_size = 100;
show_octree = false;
show_overlay = true;

% =========================================================================
%% .
close all
addpath(genpath('matlab_utilities'))

t_map_tru_active = zeros(3,1); % [0; 0; -2.0];
R_map_tru_active = eye(3);

if load_data
    disp('Extracting data from files...')
    mapfile = ['../logs/' mapname '.txt'];
    mapdata = load(mapfile)';
    if size(mapdata,2) > num_points
        mapdata = downsample(mapdata, num_points, 2);
    end

    mapdata = R_map_tru_active * mapdata + t_map_tru_active;

    trufile = 'pcl/sas_pc_0.bin';
    trudata = read_log(trufile, 3, false);
    trudata = downsample(trudata, num_points, 2)';
    i = 1;
    while i <= size(trudata, 1)
        if trudata(i,3) < 0.1
            trudata(i,:) = [];
        else
            i = i + 1;
        end
    end

    disp('Constructing True Map OcTree...')
    OTtru = OcTree(trudata,'binCapacity',bin_size,'style','weighted');
    OTtru.shrink
    % sum(sum(~isinf(OTtru.BinAverages))/3) % number of valid nodes
end

%% .
disp('Initializing evaluation nodes...')
% Initialize evaluation struct--one bin for every leaf node in the truth
% octree
numEvalNodes = 0;
evalKey = zeros(1, OTtru.BinCount);
for i = 1:1:OTtru.BinCount
    if ~isempty(OTtru.Nodes(i).points) % is a leaf node
        numEvalNodes = numEvalNodes + 1;
        evalKey(i) = numEvalNodes;
        evalNodes(numEvalNodes).bin = i;
        evalNodes(numEvalNodes).covered = false;
        evalNodes(numEvalNodes).error = 0;
        evalNodes(numEvalNodes).bounds.xmin = OTtru.BinBoundaries(i,1);
        evalNodes(numEvalNodes).bounds.xmax = OTtru.BinBoundaries(i,4);
        evalNodes(numEvalNodes).bounds.ymin = OTtru.BinBoundaries(i,2);
        evalNodes(numEvalNodes).bounds.ymax = OTtru.BinBoundaries(i,5);
        evalNodes(numEvalNodes).bounds.zmin = OTtru.BinBoundaries(i,3);
        evalNodes(numEvalNodes).bounds.zmax = OTtru.BinBoundaries(i,6);
    end
end

disp('Computing coverage and error metrics...')
% Use the estimated map points to determine coverage and error metrics
for i = 1:1:size(mapdata,2)
    [p, d, b] = OTtru.getClosestPointInTree(mapdata(:,i));
    evalNodes(evalKey(b)).covered = true;
    evalNodes(evalKey(b)).error = [evalNodes(evalKey(b)).error d];
end

disp('Plotting...')
if show_octree
    plot_octree(OTtru)
    figure
    subplot(2,4,1)
    plot_octree_search(OTtru, [-1.0;1.0;1.0])
    subplot(2,4,2)
    plot_octree_search(OTtru, [-1.0;1.0;0.0])
    subplot(2,4,3)
    plot_octree_search(OTtru, [-4.0;4.0;2.0])
    subplot(2,4,4)
    plot_octree_search(OTtru, [-4.0;4.0;1.0])
    subplot(2,4,5)
    plot_octree_search(OTtru, [32*rand-16;32*rand-16;4*rand])
    subplot(2,4,6)
    plot_octree_search(OTtru, [32*rand-16;32*rand-16;4*rand])
    subplot(2,4,7)
    plot_octree_search(OTtru, [32*rand-16;32*rand-16;4*rand])
    subplot(2,4,8)
    plot_octree_search(OTtru, [32*rand-16;32*rand-16;4*rand])
end

if show_overlay
    figure
    plot3(trudata(:,1),trudata(:,2),trudata(:,3),'k.')
    hold on; grid on
    plot3(mapdata(1,:),mapdata(2,:),mapdata(3,:),'r.')
    hold off
    xlim([-16 16])
    ylim([-16 16])
    zlim([0 4])
    pbaspect([8 8 1])
end

avg_errors = [];
num_covered = 0;
figure
for i = 1:1:numEvalNodes
    plot_eval_node(evalNodes(i))
    avg_error = mean(evalNodes(i).error);
    if avg_error > 0
        avg_errors = [avg_errors avg_error];
    end
    if evalNodes(i).covered
        num_covered = num_covered + 1;
    end
    hold on
end
hold off
xlim([-16 16])
ylim([-16 16])
zlim([0 3])
view(3)
% axis vis3d
pbaspect([8 8 1])
rotate3d

results.avgError = mean(avg_errors);
results.coveragePercentage = 100 * num_covered / numEvalNodes;
results

figure
p = histogram(avg_errors);
p.BinLimits = [0.0 3.0];
p.BinWidth = 0.1;
grid on
xlabel('Error (m)')
ylabel('Number of Voxels')


function [] = plot_eval_node(eval_node)
vert = [eval_node.bounds.xmin eval_node.bounds.ymin eval_node.bounds.zmin; ...
        eval_node.bounds.xmax eval_node.bounds.ymin eval_node.bounds.zmin; ...
        eval_node.bounds.xmax eval_node.bounds.ymax eval_node.bounds.zmin; ...
        eval_node.bounds.xmin eval_node.bounds.ymax eval_node.bounds.zmin; ...
        eval_node.bounds.xmin eval_node.bounds.ymin eval_node.bounds.zmax; ...
        eval_node.bounds.xmax eval_node.bounds.ymin eval_node.bounds.zmax; ...
        eval_node.bounds.xmax eval_node.bounds.ymax eval_node.bounds.zmax; ...
        eval_node.bounds.xmin eval_node.bounds.ymax eval_node.bounds.zmax];
fac = [1 2 6 5;2 3 7 6;3 4 8 7;4 1 5 8;1 2 3 4;5 6 7 8];
patch('Vertices',vert,'Faces',fac,...
      'FaceVertexCData',error_box_color_spec(eval_node.covered,mean(eval_node.error),0.01,2.0),...
      'FaceColor','flat','FaceAlpha',0.25,'EdgeAlpha',0)
end

function colorspec = error_box_color_spec(occupied, error, min_error, max_error)

if occupied
    if error < min_error
        error = min_error;
    elseif error > max_error
        error = max_error;
    end
    c = (error - min_error) / (max_error - min_error);
    cs = [c 1-c 2*c*(1-c)];
else
    cs = [0 0 0];
end
colorspec = repmat(cs, 6, 1);

end

function [] = plot_octree_search(octree, point)

plot3(octree.Points(:,1), octree.Points(:,2), octree.Points(:,3), 'k.')
hold on; grid on
[p, ~, ~] = octree.getClosestPointInTree(point);
plot3(point(1), point(2), point(3), 'r*', 'linewidth', 20)
plot3(p(1), p(2), p(3), 'g*', 'linewidth', 20)
hold off
% pbaspect([8 8 1])

end

function [] = plot_octree(octree)

figure
boxH = octree.plot;
cols = lines(octree.BinCount);
doplot3 = @(p,varargin)plot3(p(:,1),p(:,2),p(:,3),varargin{:});
for i = 1:octree.BinCount
    if octree.BinDepths(i) == 4
        set(boxH(i),'Color',cols(i,:),'LineWidth', 1+octree.BinDepths(i))
        doplot3(octree.Points(octree.PointBins==i,:),'.','Color',cols(i,:))
    end
end
axis image, view(3)

end
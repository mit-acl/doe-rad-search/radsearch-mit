bagfile = '../logs/traj_following.bag';
addpath(genpath('matlab_utilities/'));
bagdata = processAllROSBagTopics(bagfile, false);

goal = bagdata.SQ01s.goal;
t_g = goal.t;
x_g = goal.pos(1,:);
y_g = goal.pos(2,:);
z_g = goal.pos(3,:);
u_g = goal.vel(1,:);
v_g = goal.vel(2,:);
w_g = goal.vel(3,:);
psi_g = goal.yaw*180/pi;
r_g = goal.dyaw*180/pi;

state = bagdata.SQ01s.state;
t_s = state.t;
x_s = state.pos(1,:);
y_s = state.pos(2,:);
z_s = state.pos(3,:);
u_s = state.vel(1,:);
v_s = state.vel(2,:);
w_s = state.vel(3,:);
psi_s = state.euler(3,:)*180/pi;
r_s = state.w(3,:)*180/pi;

figure(1)
subplot(4,2,1)
plot(t_g, x_g, 'r--', t_s, x_s, 'k-')
ylabel('x')
grid on
legend('goal','actual')
subplot(4,2,2)
plot(t_g, u_g, 'r--', t_s, u_s, 'k-')
ylabel('u')
grid on
subplot(4,2,3)
plot(t_g, y_g, 'r--', t_s, y_s, 'k-')
ylabel('y')
grid on
subplot(4,2,4)
plot(t_g, v_g, 'r--', t_s, v_s, 'k-')
ylabel('v')
grid on
subplot(4,2,5)
plot(t_g, z_g, 'r--', t_s, z_s, 'k-')
ylabel('z')
grid on
subplot(4,2,6)
plot(t_g, w_g, 'r--', t_s, w_s, 'k-')
ylabel('w')
grid on
subplot(4,2,7)
plot(t_g, psi_g, 'r--', t_s, psi_s, 'k-')
ylabel('\psi')
grid on
subplot(4,2,8)
plot(t_g, r_g, 'r--', t_s, r_s, 'k-')
ylabel('r')
grid on
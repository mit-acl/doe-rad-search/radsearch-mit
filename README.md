# Collaborative SLAM for Radiological Search

## Setup

### Initial Setup (Universal)

After cloning the repo, navigate to the root directory and carry out the following:

1. *Install system dependencies*:

```bash
./update.sh dependencies
```

2. *Obtain ROS packages*:

```bash
./update.sh packages
```

3. *Obtain the lastest Flightmare version*:

```bash
./update.sh flightmare
```

4. *Point the simulator to the downloaded Flightmare version*:

```bash
./update.sh flightmare-pointer
```

The above four steps can be repeated any time in the future to keep things up-to-date.

### Initial Setup (Single-Agent Simulation)

After completing the universal setup steps, carry out the following:

1. *Build the software-in-the-loop ROS packages*:

```bash
cd workspaces/sas_sim__ws
catkin build snap_sil_ros
```

2. *Build the single-agent SLAM packages in order*:

```bash
catkin build opencv3_catkin
catkin build opengv_catkin
catkin build dbow2_catkin
catkin build kimera_vio_ros
```

3. *Build the rest of the ROS packages*:

```bash
source sourceror.sh
rosmake # for Release build, OR
rosmake_debug # for Debug build
```

## Usage

### Repository Tools

**update shell script**:

```bash
usage: update.sh [CMD]

CMD options:
  * dependencies
    - Will install system dependencies. Assumes you already have ROS 
      desktop version installed.
  * packages
    - Will recursively update git submodules in packages/ and make sure
      their package dependencies are up-to-date.
  * flightmare
    - Will check https://github.com/uzh-rpg/flightmare/releases for the
      newest Unity environment release and give the option to download.
  * flightmare-pointer.
    - Will give the option to change the flightmare executable your sim
      points to.
  * apriori-path PLANNER_NAME SETTINGS_NAME
    - Will use PLANNER_NAME (callable from [packages/trajectory_generation/
      planners/PLANNER_NAME/main.py]) and SETTINGS_NAME (callable from 
      [packages/radsearch_config/param/planning/PLANNER_NAME/SETTINGS_NAME.yaml]) 
      to generate waypoint file [packages/trajectory_generation/waypoints/
      PLANNER_NAME-SETTINGS_NAME.txt] and video file [logs/
      PLANNER_NAME-SETTINGS_NAME.mp4].
```

### Usage (Single-Agent Simulation)

The roslaunch command and arg list defaults are

```bash
source workspaces/sas_sim_ws/sourceror.sh
roslaunch radsearch_sim single_agent_slam.launch
```

- *launch\_unity* (true): ...
- *agent\_param* (sim_sas): ...
- *env\_param* (sim_sas): ...
- *run\_slam* (true): ...
- *run\_mesh* (true): ...
- *log\_dynamics* (false): ...
- *log\_slam* (false): ...
- *stereo\_vis* (false): ...

**Joystick vs. Trajectory Following**

To operate the UAV with a joystick, simply plug in a joystick and set the "joystick" parameter in the "env_param" file to *true*.

To run automated trajectory following, you can either specify *a priori* or *real-time* trajectory generation:

- *A Priori*: 
  - Set "planning/architecture" to "apriori" in the "env_param" file, as well as the appropriate names for "planning/method" and "settings/default".
  - Run the update.sh script with arg "apriori-path" (see the help menu for details).
- *Real-time*:
  - Set "planning/architecture" to "realtime" in the "env_param" file, as well as the appropriate names for "planning/method" and "settings/default".

**True vs. Estimated Kimera Mesh Construction**

In the "env_param" file, there is an optional field "frame" that can follow the "height" field for the first camera of the first UAV. If the "frame" field is set to "snap_cam", then UAV state truth will be used for mesh construction. Otherwise, the frame "left_cam" will be used, which corresponds to kimera's estimate. Thus, the mesher can be run without the main slam module *only if* "frame" is set to "snap_cam".

**Changing Physical Parameters**

++++
#!/bin/bash

catkin clean -y

catkin build snap_sil_ros
catkin build opencv3_catkin
catkin build opengv_catkin
catkin build dbow2_catkin
catkin build kimera_vio_ros
source sourceror.sh
ros_make

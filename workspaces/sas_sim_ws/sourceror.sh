#!/bin/bash

s_name='sas_sim_ws'

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
source "$DIR/devel/setup.bash"

echo -e "\e[1m$s_name workspace sourced\e[0m"

GREEN="\[\033[32m\]"
RESET="\[$(tput sgr0)\]"
rosenv="${GREEN}($s_name)"

if [ "$SOURCED_WS" == 'TRUE' ]
then
    echo -e "\e[36mprevious sourcing overwritten\e[0m"
else
    export PS1="${PS1}${rosenv} ${RESET}"
    export SOURCED_WS='TRUE'
fi

alias rosmake="catkin build -DCMAKE_EXPORT_COMPILE_COMMANDS=1"
alias rosmake_debug="catkin build --cmake-args -DCMAKE_BUILD_TYPE=Debug"

rosmake_gencc() {
  echo_yellow "Generating centralized compile_commands.json in build directory..."
  cd `catkin locate --workspace $(pwd)`
  concatenated="build/compile_commands.json"
  echo "[" > $concatenated
  first=1
  for d in build/*
  do
    f="$d/compile_commands.json"
    if test -f "$f"; then
      if [ $first -eq 0 ]; then
        echo "," >> $concatenated
      fi
      cat $f | sed '1d;$d' >> $concatenated
    fi
    first=0
  done
  echo "]" >> $concatenated
  echo_green "Done."
}

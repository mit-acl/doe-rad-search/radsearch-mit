#!/usr/bin/python3

import argparse, sys, yaml, os

from frontier_planner import FrontierPlanner
from occupancy_grid import OccupancyGrid
from point_cloud import read_point_cloud_log, PointCloud2D, PointCloud3D 
from robot import Robot

if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--yaml_file', type=str, default = '')
    arg_parser.add_argument('--pcl_path', type=str, default = "")
    arg_parser.add_argument('--generate_figs', type=bool, default = False)
    arg_parser.add_argument('--figure_dir', type=str, default = '')
    arg_parser.add_argument('--output', type=str, default = "waypoints.txt")
    args = arg_parser.parse_args()

    # Get yaml arguments
    yaml_args = dict()
    with open(args.yaml_file, 'r') as stream:
        yaml_args = yaml.safe_load(stream)

    # Transform the provided 3D point cloud to a 2D grid representation.
    pcl_3d = PointCloud3D(read_point_cloud_log(os.path.join(args.pcl_path, yaml_args['pcl_map_name'] + '.bin'), 3, False))
    pcl_2d = pcl_3d.to_2D(yaml_args['z_min'], yaml_args['z_max'])
    map_grid = OccupancyGrid(pcl_2d, yaml_args['grid_resolution'])

    # Create a robot within the grid world.
    robot = Robot(yaml_args['start_x'], yaml_args['start_y'], 
                  yaml_args['robot_radius'], 
                  yaml_args['robot_yaw'],
                  yaml_args['robot_sensor_range'], 
                  yaml_args['robot_sensor_view_angle'])

    # Plan a full coverage path for the robot to explore the gridworld.
    planner = FrontierPlanner(map_grid, robot)
    waypoints = planner.get_full_coverage_path(plot=args.generate_figs, figdir=args.figure_dir)

    # Write the planned waypoints out to a file for later use.
    with open(args.output, "w") as f:
        for p in waypoints:
            f.write(",".join([str(v) for v in p]))
            f.write("\n")

import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np
from enum import Enum


class OccupancyGrid2D():
    def __init__(self, data: np.ndarray, rows: int, cols: int):
        assert data.shape[1] == 2

        self.data = data
        self.rows = rows
        self.cols = cols
        self.x_min = np.min(data[:, 0])
        self.x_max = np.max(data[:, 0])
        self.x_step = (self.x_max - self.x_min) / float(cols - 1)
        self.x_min = np.min(data[:, 0]) - (self.x_step / 2.0)
        self.x_max = np.max(data[:, 0]) + (self.x_step / 2.0)

        self.y_min = np.min(data[:, 1])
        self.y_max = np.max(data[:, 1])
        self.y_step = (self.y_max - self.y_min) / float(rows - 1)
        self.y_min = np.min(data[:, 1]) - (self.y_step / 2.0)
        self.y_max = np.max(data[:, 1]) + (self.y_step / 2.0)

        self.grid = np.zeros((rows, cols))
        for i in range(cols):
            for j in range(rows):
                x_points = np.logical_and(self.data[:, 0] > self.x_min + i * self.x_step, 
                                          self.data[:, 0] <= self.x_min + (i + 1) * self.x_step)
                y_points = np.logical_and(self.data[:, 1] > self.y_min + j * self.y_step, 
                                          self.data[:, 1] <= self.y_min + (j + 1) * self.y_step)

                cell_points = self.data[np.logical_and(x_points, y_points), :].shape[0]

                if cell_points > 0:
                    self.grid[i, j] = 1



    def plot(self, ax):
        for i in range(self.cols):
            for j in range(self.rows):
                if self.grid[i, j] == 1:
                    rect = patches.Rectangle((self.x_min + (i * self.x_step), 
                                             self.y_min + (j * self.y_step)),
                                             self.x_step,
                                             self.y_step,
                                             linewidth = 1,
                                             color='g',
                                             fill = True,
                                             alpha=0.2)
                    ax.add_patch(rect)

        ax.set_xticks([], minor=False)
        ax.set_yticks([], minor=False)
        ax.set_xticks(np.arange(self.x_min, self.x_max, step=self.x_step), minor=True)
        ax.set_yticks(np.arange(self.y_min, self.y_max, step=self.y_step), minor=True)
        ax.grid(which='minor', alpha=0.5)



class PointCloud2D():
    def __init__(self, data: np.ndarray):
        assert (data.shape[1] == 2)
        self.data = data


    def plot(self, ax, sample_rate: float = 0.1):
        plot_idx = np.random.choice(np.arange(self.data.shape[0]), 
                                    int(self.data.shape[0] * sample_rate),
                                    replace = False)
        
        ax.scatter(self.data[plot_idx, 0], 
                    self.data[plot_idx, 1],
                    s = 0.5)


class PointCloud3D():
    def __init__(self, data: np.ndarray):
        assert (data.shape[1] == 3)
        self.data = data

    def to_2D(self, z_min: float, z_max: float) -> PointCloud2D:
        mask = np.logical_and(self.data[:, 2] >= z_min, self.data[:, 2] <= z_max)
        return PointCloud2D(self.data[mask, :2])


def read_point_cloud_log(path: str, row_size: int, double_precision: bool = True) -> np.ndarray:
    with open(path, 'rb') as f:
        data_type =  np.double if double_precision else np.single
        data = np.fromfile(f, data_type)
        return data.reshape((-1, row_size))


if __name__ == "__main__":
    pcl_3d = PointCloud3D(read_point_cloud_log("pcl/sas_pc_0.bin", 3, False))
    pcl_2d = pcl_3d.to_2D(0.5, 0.6)
    grid_dim = 100
    map_grid = OccupancyGrid2D(pcl_2d.data, grid_dim, grid_dim)

    f = plt.figure()
    ax = f.add_subplot(1, 1, 1)
    pcl_2d.plot(ax, 0.1)
    map_grid.plot(ax)
    plt.show()
    
    
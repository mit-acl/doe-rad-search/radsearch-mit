#include <trajectory_generation.h>

class WaypointFollower {
  [[maybe_unused]] ros::Subscriber currentStateSub;
  ros::Publisher desiredStatePub;
  ros::Publisher statusTextPub;
  ros::Timer desiredStateTimer;

  ros::Time trajectoryStartTime;
  mav_trajectory_generation::Trajectory trajectory;
  mav_trajectory_generation::Trajectory yaw_trajectory;
  
  Eigen::Vector3d x;  // current position of the UAV's c.o.m. in the world frame

  visualization_msgs::Marker status_text;

  double v_max;
  double a_max;

  void onCurrentState(snapstack_msgs::State const& cur_state) {
    x << cur_state.pos.x,
         cur_state.pos.y,
         cur_state.pos.z;
  }

  void publishDesiredState(ros::TimerEvent const& ev) {
    if (trajectory.empty()) return;

    snapstack_msgs::Goal next_point;
    ros::Duration sample_duration(0.01);

    ros::Duration time_from_start = ros::Time::now() + sample_duration - trajectoryStartTime;
    double sampling_time = time_from_start.toSec();

    if (sampling_time > trajectory.getMaxTime())
      sampling_time = trajectory.getMaxTime();

    // Getting the desired state based on the optimized trajectory we found.
    using namespace mav_trajectory_generation::derivative_order;
    Eigen::Vector3d des_p = trajectory.evaluate(sampling_time, POSITION);
    Eigen::Vector3d des_v = trajectory.evaluate(sampling_time, VELOCITY);
    Eigen::Vector3d des_a = trajectory.evaluate(sampling_time, ACCELERATION);
    Eigen::Vector3d des_j = trajectory.evaluate(sampling_time, JERK);
    next_point.yaw = yaw_trajectory.evaluate(sampling_time, ORIENTATION)[0];

    next_point.p.x = des_p[0];
    next_point.p.y = des_p[1];
    next_point.p.z = des_p[2];

    next_point.v.x = des_v[0];
    next_point.v.y = des_v[1];
    next_point.v.z = des_v[2];

    next_point.a.x = des_a[0];
    next_point.a.y = des_a[1];
    next_point.a.z = des_a[2];

    next_point.j.x = des_j[0];
    next_point.j.y = des_j[1];
    next_point.j.z = des_j[2];

    next_point.mode_xy = snapstack_msgs::Goal::MODE_POSITION_CONTROL;
    next_point.mode_z  = snapstack_msgs::Goal::MODE_POSITION_CONTROL;

    next_point.power = true;
    // ROS_INFO_THROTTLE(2, "Traversed %f percent of the trajectory.",
    //          sampling_time / trajectory.getMaxTime() * 100);
    std::stringstream status_ss;
    status_ss << "Traversed " << static_cast<int>(sampling_time / trajectory.getMaxTime() * 100)
              << " percent of the trajectory.";
    status_text.text = status_ss.str();
    statusTextPub.publish(status_text);

    desiredStatePub.publish(next_point);
  }

 public:
  explicit WaypointFollower(ros::NodeHandle& nh) {

    v_max = nh.param<double>("trajectory_generation/v_max", 0.0);
    a_max = nh.param<double>("trajectory_generation/a_max", 0.0);


    currentStateSub = nh.subscribe(
        "state", 1, &WaypointFollower::onCurrentState, this);

    desiredStatePub = nh.advertise<snapstack_msgs::Goal>("goal", 1);
    statusTextPub = nh.advertise<visualization_msgs::Marker>("trajectory_status", 1);

    status_text.header.frame_id = "world";
    status_text.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
    status_text.action = visualization_msgs::Marker::ADD;
    status_text.pose.position.x = 0;
    status_text.pose.position.y = 0;
    status_text.pose.position.z = 10;
    status_text.color.r = 1.0;
    status_text.color.g = 1.0;
    status_text.color.b = 1.0;
    status_text.color.a = 1.0;
    status_text.scale.z = 0.75;

    desiredStateTimer = nh.createTimer(
        ros::Rate(50), &WaypointFollower::publishDesiredState, this);
    desiredStateTimer.start();
  }

  void generateOptimizedTrajectory(geometry_msgs::PoseArray const& poseArray) {
    ROS_INFO("[trajectory generator] Desired trajectory vertices received.");
    if (poseArray.poses.size() < 1) {
      ROS_ERROR("Must have at least one pose to generate trajectory!");
      trajectory.clear();
      yaw_trajectory.clear();
      return;
    }

    if (!trajectory.empty()) {
      ROS_WARN("Trajectory not empty! Returning..."); 
      return;
    }

    const int D_pos = 3;  // dimension of each vertex in the trajectory
    mav_trajectory_generation::Vertex start_position(D_pos), end_position(D_pos);
    mav_trajectory_generation::Vertex::Vector vertices;

    const int D_yaw = 1;
    mav_trajectory_generation::Vertex start_yaw(D_yaw), end_yaw(D_yaw);
    mav_trajectory_generation::Vertex::Vector yaw_vertices;

    // ============================================
    // Convert the pose array to a list of vertices
    // ============================================

    // Start from the current position and zero orientation
    using namespace mav_trajectory_generation::derivative_order;

    start_position.makeStartOrEnd(x, SNAP);
    vertices.push_back(start_position);

    start_yaw.addConstraint(ORIENTATION, 0);
    yaw_vertices.push_back(start_yaw);

    double yaw_prev = 0;
    for (auto i = 0; i < poseArray.poses.size(); i++)
    {
      // Populate vertices (for the waypoint positions)
      geometry_msgs::Pose pose = poseArray.poses[i];
      Eigen::Vector3d position(pose.position.x, pose.position.y, pose.position.z);
      mav_trajectory_generation::Vertex waypoint(D_pos);

      if (i != (poseArray.poses.size() - 1))
      {
        waypoint.addConstraint(POSITION, position);
        vertices.push_back(waypoint);
      }
      else
      {
        end_position.makeStartOrEnd(position, SNAP);
        vertices.push_back(end_position);
      }

      // Populate yaw_vertices (for the waypoint yaw angles)
      mav_trajectory_generation::Vertex yaw_waypoint(D_yaw);

      // Ensure a smooth commanded yaw trajectory despite angle wrapping
      double yaw = tf2::getYaw(pose.orientation);
      if (abs(yaw - yaw_prev) > M_PI)
        yaw += 2 * M_PI * static_cast<double>((0 < (yaw_prev - yaw)) - ((yaw_prev - yaw) < 0));

      yaw_prev = yaw;
      yaw_waypoint.addConstraint(ORIENTATION, yaw);

      yaw_vertices.push_back(yaw_waypoint);
    }

    // ============================================================
    // Estimate the time to complete each segment of the trajectory
    // ============================================================

    // HINT: play with these segment times and see if you can finish
    // the race course faster!
    std::vector<double> segment_times;
    segment_times = estimateSegmentTimes(vertices, v_max, a_max);
    // Ensure that no segment times are zero; instead, set zero-length segment times to the smallest nonzero segment time
    double min_segment_time = 1.0e10;
    for (auto segment_time : segment_times)
      if (segment_time < min_segment_time && segment_time > 0)
        min_segment_time = segment_time;
    for (int i = 0; i < segment_times.size(); i++)
      if (segment_times[i] == 0)
        segment_times[i] = min_segment_time;
    // =====================================================
    // Solve for the optimized trajectory (linear optimizer)
    // =====================================================
    // Position
    const int N = 10;
    mav_trajectory_generation::PolynomialOptimization<N> opt(D_pos);
    opt.setupFromVertices(vertices, segment_times, SNAP);
    opt.solveLinear();

    // Yaw
    mav_trajectory_generation::PolynomialOptimization<N> yaw_opt(D_yaw);
    yaw_opt.setupFromVertices(yaw_vertices, segment_times, SNAP);
    yaw_opt.solveLinear();

    // ============================
    // Get the optimized trajectory
    // ============================
    // mav_trajectory_generation::Segment::Vector segments;
    //        opt.getSegments(&segments); // Unnecessary?
    opt.getTrajectory(&trajectory);
    yaw_opt.getTrajectory(&yaw_trajectory);
    trajectoryStartTime = ros::Time::now();

    ROS_INFO("Generated optimized trajectory from %d waypoints",
             vertices.size());
  }
};

int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::InstallFailureSignalHandler();
  ros::init(argc, argv, "trajectory_generation_node");
  ros::NodeHandle nh;
  std::string fpath;
  float flight_alt;
  nh.getParam("waypoint_fpath", fpath);
  nh.getParam("flight_alt", flight_alt);
  WaypointFollower waypointFollower(nh);

  trajectory_generation::WaypointPublisherService srv;
  srv.request.file_path = fpath;
  srv.request.flight_alt.data = flight_alt;
  ros::ServiceClient client = nh.serviceClient<trajectory_generation::WaypointPublisherService>("desired_traj_vertices");

  client.waitForExistence();
  if (client.call(srv)) {
    waypointFollower.generateOptimizedTrajectory(srv.response.waypoints);
  }
  else
  {
    ROS_WARN("UNABLE TO OBTAIN DESIRED TRAJECTORY VERTICES");
  }
  
  ros::spin();
  return 0;
}

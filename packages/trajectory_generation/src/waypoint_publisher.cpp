#include <waypoint_publisher.h>


bool read_waypoint_file(const std::string& file_name, const float& flight_alt,
                    geometry_msgs::PoseArray* poseArray) {
    std::ifstream f(file_name.c_str());
    std::string line;

    if(!f) {
        std::cerr << "Cannot open the File : " << file_name << std::endl;
        return false;
    }

    float x, y, yaw;
    geometry_msgs::Pose pose;

    while (std::getline(f, line)) {
        std::vector<std::string> cmd_fields;
        std::istringstream line_stream(line);

        while (line_stream.good()) {
            std::string substr;
            std::getline(line_stream, substr, ',');
            cmd_fields.push_back(substr);
        }

        x = std::stof(cmd_fields[0]);
        y = std::stof(cmd_fields[1]);
        yaw = std::stof(cmd_fields[2]);

        pose.position.x = x;
        pose.position.y = y;
        pose.position.z = flight_alt;
        tf2::Quaternion q;
        q.setRPY(0., 0., M_PI * yaw / 180.0);
        pose.orientation = tf2::toMsg(q.normalize());

        ROS_INFO("(X: %0.2f | Y: %0.2f | Z: %0.2f | YAW: %0.2f)", pose.position.x, pose.position.y, pose.position.z, yaw);
        poseArray->poses.push_back(pose);
    }

    f.close();
    return true;

}

bool publish_waypoints(trajectory_generation::WaypointPublisherService::Request &req, 
                       trajectory_generation::WaypointPublisherService::Response &resp) {
    geometry_msgs::PoseArray poses;
    read_waypoint_file(req.file_path, req.flight_alt.data, &poses);
    resp.waypoints = poses;
    return true;
}


int main(int argc, char** argv) {
    ros::init(argc, argv, "waypoint_publisher");
    ros::NodeHandle nh;
    ros::ServiceServer service = nh.advertiseService("desired_traj_vertices", publish_waypoints);
    ros::spin();
    return 0;
}

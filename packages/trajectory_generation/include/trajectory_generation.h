#include <eigen3/Eigen/Dense>
#include <eigen_conversions/eigen_msg.h>
#include <geometry_msgs/PoseArray.h>
#include <mav_trajectory_generation/polynomial_optimization_linear.h>
#include <mav_trajectory_generation/trajectory.h>
#include <nav_msgs/Odometry.h>
#include <ros/ros.h>
#include <snapstack_msgs/Goal.h>
#include <snapstack_msgs/State.h>
#include <tf/transform_datatypes.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2/utils.h>
#include <trajectory_generation/WaypointPublisherService.h>
#include <visualization_msgs/Marker.h>


#!/usr/bin/python

import rospy

class CameraInfoPublisher(object):
    def __init__(self):
        pass # TODO: needed for non-flightmare camera sensors?

if __name__ == '__main__':
    rospy.init_node('cam_info_publisher', anonymous=True)
    CIP = CameraInfoPublisher()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print('Shutting down camera info publisher node.')
#!/usr/bin/python

import sys, yaml, os

def getParamFromCmd(param_def, cmd, arg_list):
    if cmd == "get_sensor_frame":
        first_camera = param_def['vehicle_definitions'][0]['cameras'][0]
        if 'frame' in first_camera:
            camera_frame = first_camera['frame']
        else:
            camera_frame = 'left_cam'
        return camera_frame
    elif cmd == "get_meshfilename":
        meshname = 'mesh'
        if 'planning' in param_def:
            if 'method' in param_def['planning'] and 'settings' in param_def['planning']:
                meshname = '%s-%s' % (param_def['planning']['method'], param_def['planning']['settings'])
        if 'frame' in param_def['vehicle_definitions'][0]['cameras'][0]:
            meshname = meshname + '-' + param_def['vehicle_definitions'][0]['cameras'][0]['frame']
        return os.path.join(arg_list[0], meshname + '.ply')
    elif cmd == "get_wp_fname":
        wpname = 'waypoints'
        if 'planning' in param_def:
            if 'method' in param_def['planning'] and 'settings' in param_def['planning']:
                wpname = '%s-%s' % (param_def['planning']['method'], param_def['planning']['settings'])
        return os.path.join(arg_list[0], wpname + '.txt')
    else:
        return ""

with open(sys.argv[1]) as f:
    print(getParamFromCmd(yaml.load(f, Loader=yaml.Loader), sys.argv[2], sys.argv[3:]))
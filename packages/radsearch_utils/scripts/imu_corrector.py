#!/usr/bin/python

import rospy
from sensor_msgs.msg import Imu

class ImuCorrector(object):
    def __init__(self):
        self.input_sub = rospy.Subscriber('imu_input', Imu, self.inputCallback)
        self.output_pub = rospy.Publisher('imu_output', Imu, queue_size=1)

        # Which IMU frame conversion to use
        self.conv = self.conv_Snap2Imu # <<<<

    def inputCallback(self, msg):
        imu_out = Imu()
        imu_out.header = msg.header
        # don't think the orientation field is used by kimera...
        imu_out.orientation.w = 1.0
        imu_out.orientation.x = 0.0
        imu_out.orientation.y = 0.0
        imu_out.orientation.z = 0.0
        imu_out.linear_acceleration.x, imu_out.linear_acceleration.y, imu_out.linear_acceleration.z = \
          self.conv(msg.linear_acceleration.x, msg.linear_acceleration.y, msg.linear_acceleration.z)
        imu_out.angular_velocity.x, imu_out.angular_velocity.y, imu_out.angular_velocity.z = \
          self.conv(msg.angular_velocity.x, msg.angular_velocity.y, msg.angular_velocity.z)
        self.output_pub.publish(imu_out)

    # def conv_Snap2EuRoC(self, x_in, y_in, z_in):
    #     # Apply R_S^E to the vector [x_in, y_in, z_in]
    #     x_out = z_in
    #     y_out = -y_in
    #     z_out = x_in
    #     return x_out, y_out, z_out

    def conv_I(self, x_in, y_in, z_in):
        x_out = x_in
        y_out = y_in
        z_out = z_in
        return x_out, y_out, z_out

    def conv_Snap2EuRoC(self, x_in, y_in, z_in):
        # Apply R_S^E to the vector [x_in, y_in, z_in]
        x_out = -y_in
        y_out = z_in
        z_out = -x_in
        return x_out, y_out, z_out

    def conv_Snap2Imu(self, x_in, y_in, z_in):
        # R_S^I (from Kimera paper)
        x_out = -y_in
        y_out = x_in
        z_out = z_in
        return x_out, y_out, z_out

    # def conv_Snap2Kimera(self, x_in, y_in, z_in):
    #     # Apply R_S^K to the vector [x_in, y_in, z_in]
    #     x_out = z_in
    #     y_out = -x_in
    #     z_out = -y_in
    #     return x_out, y_out, z_out

    def conv_Snap2Kimera(self, x_in, y_in, z_in):
        # Apply R_S^K to the vector [x_in, y_in, z_in]
        x_out = -x_in
        y_out = -z_in
        z_out = -y_in
        return x_out, y_out, z_out

if __name__ == '__main__':
    rospy.init_node('imu_corrector', anonymous=True)
    IC = ImuCorrector()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print 'Shutting down IMU corrector node.'
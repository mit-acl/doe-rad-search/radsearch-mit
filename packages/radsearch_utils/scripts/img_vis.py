#!/usr/bin/python

import rospy, os, cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np

def depthImageCallback(msg):
    bridge = CvBridge()
    img = bridge.imgmsg_to_cv2(msg, "passthrough")
    cv2.imwrite(os.path.join(os.path.expanduser("~"), "depth_imgs", "depth_%d.png" % msg.header.seq), img)

def rgbImageCallback(msg):
    bridge = CvBridge()
    img = bridge.imgmsg_to_cv2(msg, "passthrough")
    cv2.imwrite(os.path.join(os.path.expanduser("~"), "depth_imgs", "rgb_%d.png" % msg.header.seq), img)

if __name__ == '__main__':
    rospy.init_node('img_vis', anonymous=True)
    depth_img_sub = rospy.Subscriber('depth_image', Image, depthImageCallback, queue_size=1)
    rgb_img_sub = rospy.Subscriber('rgb_image', Image, rgbImageCallback, queue_size=1)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print('Shutting down img_vis node.')